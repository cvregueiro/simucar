# simucar

ROS Gazebo project  for simulating a f1 car on a circuit with three type of sensors: cameras, RGB-D camera and a LIDAR.


## Install

Copy or clone package:
```
  git clone https://gitlab.com/cvregueiro/simucar
```

Change to the package directory
```
  cd simucar_ws
```

If it is neccessary, load the environmental variables
```
  source /opt/ros/melodic/setup.bash
```

Compile as usual 
```
  catkin_make
```

Install
```
  catkin_make install
  source devel/setup.bash
```

## Execute

There are 2 options to launch Gazebo simulator: with or without RGB-D Kinect camera
```
  roslaunch simucar ff_laser_rgbd.launch
```
 or
```
  roslaunch simucar ff_laser.launch
```

In some instalations, to work properly, a copy of the contents of directories world and models to $(HOME)/.gazebo it is needed:
```
   cp -R  src/simucar/worlds/*  ~/.gazebo/worlds/
   cp -R  src/simucar/models/*  ~/.gazebo/models/
```

In a second terminal (do not forget to type "souce devel/setup.bash"), execute rviz to show data. There are also 2 options
```
  roslaunch simucar f1ROS2.launch 
```
 or
```
  roslaunch simucar f1ROS.launch 
```

At right bottom, there is a virtual joystick that can control the f1 car.

To stop, type Ctrl+C in each terminal


### To reset Gazebo simulation, type in another terminal
```
  rosservice call /gazebo/reset_simulation "{}"
```

In code:
```
  std_srvs::Empty resetWorldSrv;
  ros::service::call("/gazebo/reset_world", resetWorldSrv);
```

